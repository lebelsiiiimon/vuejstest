const express = require('express');
const path = require('path');
// const bodyParser = require('body-parser');
// const cors = require('cors');

const app = express();

// Port Number
const port = process.env.PORT || 8080;

// Set Static Folder
app.use(express.static(path.join(__dirname, '/dist')));

// // Index Route
// app.get('/', (req, res) => {
//   res.send('invaild endpoint');
// });
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

// Start Server
app.listen(port, () => {
  console.log('Server started on port ', port);
});
